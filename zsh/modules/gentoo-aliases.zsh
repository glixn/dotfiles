# emerge aliases
alias einstall="emerge --ask --verbose"
alias esearch="emerge --search"
alias eupdate="emerge -av1 --update --deep @world"
alias esync="emerge --sync"

# other scripts
alias rmus="/usr/sscripts/rmus"
